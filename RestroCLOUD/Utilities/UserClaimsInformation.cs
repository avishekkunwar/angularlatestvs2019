﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestroCLOUD.Utilities
{
    public class UserClaimsInformation
    {
        public string UserID { get; set; }
        public string Email { get; set; }
    }
}
