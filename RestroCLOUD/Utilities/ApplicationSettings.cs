﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestroCLOUD.Utilities
{
    public class ApplicationSettings
    {
        public string JWT_SecretKey { get; set; }
        public string ServerURL { get; set; }
    }
}
