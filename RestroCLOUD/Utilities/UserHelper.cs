﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RestroCLOUD.Utilities
{
    public static class UserHelper
    {
        public static UserClaimsInformation ClaimsInformation(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated == false) return null;
            var userClaimsInformation = new UserClaimsInformation();
            foreach (var item in user.Claims)
            {
                if (item.Type == "UserID")
                {
                    //string userID = 0;
                    //string.TryParse(item.Value, out userID);
                    userClaimsInformation.UserID = item.Value;
              
                }
                else if (item.Type == "Email")
                {
                    userClaimsInformation.Email = item.Value;
                }
                
            }
            return userClaimsInformation;
        }
        }
}
