﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace RestroCLOUD.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : BaseApiController
    {
        private UserManager<ApplicationUser> _userManager { get; set; }
        public UserProfileController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
        [HttpGet]
       

        //if you dont specify route it will search for default : /api/UserProfile
        [Route("GetUserProfile")]
        public async Task<Object> GetUserProfile()
        {
            var a = CurrentUserDetails.Email;
            string UserID = User.Claims.First(x => x.Type == "UserID").Value;
            var Existeduser =await _userManager.FindByIdAsync(UserID);
            return new
            {
                Existeduser.FullName,
                Existeduser.Email,
                Existeduser.UserName
            };
        }

        [HttpGet]
        [Route("GetList")]
        public IActionResult GetList()
        {
            return Ok();
        }

        [HttpGet]
        [Authorize(Roles ="Admin")]
        [Route("ForAdmin")]
        public string GetForAdmin()
        {
            return "Web Method for Admin";
        }

        [HttpGet]
        [Authorize(Roles = "Customer")]
        [Route("ForCustomer")]
        public string GetForCustomer()
        {
            return "Web Method for Customer";
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Customer")]
        [Route("ForAdminCustomer")]
        public string GetForAdminCustomer()
        {
            return "Web Method for Admin and Customer";
        }
    }
}