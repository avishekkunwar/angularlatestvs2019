﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestroCLOUD.Utilities;

namespace RestroCLOUD.Controller
{
    [Route("api/[controller]")]
    public abstract class BaseApiController : ControllerBase
    {
        private UserClaimsInformation _currentUserDetails;
        protected virtual UserClaimsInformation CurrentUserDetails
        {
            get
            {
                if (_currentUserDetails == null)
                {
                    _currentUserDetails = User.ClaimsInformation();
                }
                return _currentUserDetails;
            }
        }
    }
}